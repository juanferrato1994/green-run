interface TitleProps {
  text: string;
  className?: string;
}

const Title = ({ text, className }: TitleProps) => (
  <h1 className={className}>{text}</h1>
);

export default Title;
