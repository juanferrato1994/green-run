interface SubtitleProps {
    text: string;
    className?: string;
  }
  
  const Subtitle = ({ text, className }: SubtitleProps) => (
    <h2 className={className}>{text}</h2>
  );
  
  export default Subtitle;
  