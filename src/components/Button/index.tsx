import { MouseEventHandler } from "react";

interface ButtonProps {
  text: string;
  onClick: MouseEventHandler<HTMLButtonElement>;
  type?: "submit" | "reset" | "button";
  className?: string;
}

const Button = ({ text, onClick, type, className }: ButtonProps) => (
  <button className={className} onClick={onClick} type={type}>
    {text}
  </button>
);

export default Button;
