export interface Sport {
    idSport: string;
    strFormat: string;
    strSport: string;
    strSportDescription: string;
    strSportIconGreen: string;
    strSportThumb: string;
  }
  