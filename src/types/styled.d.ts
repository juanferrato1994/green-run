import "styled-components";

declare module "styled-components" {
  export interface DefaultTheme {
    name: string;
    text: string;
    link: string;
    background: {
      dark: string;
      light: string;
      input: string;
      item: string;
    };
    input: {
      label: string;
      value: string;
    };
    bottomBanner: string;
    navbar: {
      background: string;
      selected: string;
      icon: {
        selected: string;
        unselected: string;
      };
    };
    icons: {
      like: string;
      dislike: string;
    };
    header: {
      backArrow: string;
    };
    themeButtonBackground: string;
    candidateNameText: string;
  }
}
