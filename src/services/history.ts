import {
  doc,
  collection,
  setDoc,
  getDocs,
  query,
  orderBy,
} from "firebase/firestore";
import { db } from "../firebase";
import { Sport } from "../types";

export type Reaction = "like" | "dislike";

export type UID = string;

interface RegisterReactionProps {
  uid: UID;
  data: {
    sport: Sport;
    reaction: Reaction;
  };
}

// interface ReactionItem {
//   date: Date;
//   reaction: Reaction;
//   sport: Sport;
// }

export const registerReaction = async ({
  uid,
  data,
}: RegisterReactionProps) => {
  const historyRef = collection(db, "history/" + uid + "/reactions");

  await setDoc(doc(historyRef), {
    ...data,
    date: Date.now(),
  }).then(() => {});
};

export const getReactionHistory = async ({ uid }: { uid: UID }) => {
  const reactionsRef = collection(db, "history/" + uid + "/reactions");
  const q = query(reactionsRef, orderBy("date", "desc"));
  const querySnapshot = await getDocs(q);
  let reactions: any[] = [];

  querySnapshot.docs.forEach((doc) => {
    if (doc.data()) {
      reactions.push(doc.data());
    }
  });

  return reactions;
};
