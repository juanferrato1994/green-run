import styled, { useTheme } from "styled-components";
import SvgIconWrapper from "../SvgIconWrapper";
import { ReactComponent as Heart } from "../../assets/svg/heart.svg";
import { Colors } from "../../themes/themes";

interface LikeButtonProps {
  onPress: any;
}

const LikeButton = ({ onPress }: LikeButtonProps) => {
  const theme = useTheme();

  return (
    <ButtonContainer onClick={onPress}>
      <Icon height={35} fill={Colors.white} stroke={theme.icons.like}>
        <Heart />
      </Icon>
    </ButtonContainer>
  );
};

export default LikeButton;

const ButtonContainer = styled.button`
    border: none;
    background: transparent;
    margin: 0 11px;
`;


const Icon = styled(SvgIconWrapper)`
  border-radius: 50%;
  width: 81px;
  height: 81px;
  display: flex;
  justify-content: center;
  align-items: center;
  box-shadow: 0px 10px 25px rgba(35, 107, 254, 0.2);
  background: linear-gradient(125.02deg, #236bfe -17.11%, #063ba8 98.58%);
`;