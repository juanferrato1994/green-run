import { MouseEvent } from "react";

import styled, { useTheme } from "styled-components";
import SvgIconWrapper from "../SvgIconWrapper";
import { useNavigate } from 'react-router-dom';
import { ReactComponent as ArrowLeft } from "../../assets/svg/arrow-left.svg";

const GoBackArrow = () => {
  const previousPage = -1;
  const theme = useTheme();
  const navigate = useNavigate();

  function handleGoBack(e: MouseEvent<HTMLButtonElement>) {
    try {
      navigate(previousPage);
    } catch (error) {
      // manage error
    }
  }

  return (
    <ButtonContainer onClick={handleGoBack}>
      <Icon
        height={40}
        fill={"none"}
        stroke={theme.header.backArrow}
      >
        <ArrowLeft />
      </Icon>
    </ButtonContainer>
  );
};

export default GoBackArrow;

const ButtonContainer = styled.button`
  background-color: transparent;
  border: 0;
  padding: 0;
  margin: 20px 0;
`;


const Icon = styled(SvgIconWrapper)`

`;