import styled from "styled-components";
import Subtitle from "../../components/Subtitle";

export const PrimarySubtitle = styled(Subtitle)`
  font-family: "Epilogue";
  font-style: normal;
  font-weight: 400;
  font-size: 18px;
  color: ${(props) => props.theme.text};
  line-height: 26.64px;
  opacity: 0.8;
`;