import styled from "styled-components";
import Title from "../../components/Title";

export const PrimaryTitle = styled(Title)`
  font-size: 42px;
  font-weight: 700;
  color: ${(props) => props.theme.text};
  text-align: center;
  margin: 0;
  opacity: 0.8;
`;
