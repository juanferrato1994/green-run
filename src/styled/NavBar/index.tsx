import { To, useLocation, useNavigate } from "react-router-dom";
import styled, { useTheme } from "styled-components";
import SvgIconWrapper from "../SvgIconWrapper";
import { PATHS } from "../../navigation/paths";
import { useContext } from "react";
import { AuthContext } from "../../contexts/AuthContext";
import { NavbarProfileDefaultImg } from "../../assets/images";
import { House, Timer, Notepad } from "../../assets/icons";
import { animated, useSpring } from "react-spring";

interface NavBarItemProps {
  active: boolean;
}

const items = [
  { id: 0, iconName: "house", pathname: PATHS.HOME },
  { id: 1, iconName: "timer", pathname: PATHS.HISTORY },
  { id: 2, iconName: "notepad", pathname: PATHS.NOTES },
];

const Navbar = () => {
  const theme = useTheme();
  const location = useLocation();
  const navigate = useNavigate();
  const { logOut } = useContext(AuthContext);
  const currentItemAnimation = useSpring({
    from: { scale: 1 },
    to: { scale: 1.2 },
    config: {duration: 2300},
    loop: { reset: true },
  });

  function getIcon(iconName: string) {
    if (iconName === "house") return <House />;
    if (iconName === "timer") return <Timer />;
    if (iconName === "notepad") return <Notepad />;
  };

  function handleItemClick(path: To) {
    navigate(path);
  }
  

  function renderItems() {
    return items.map((item) => (
      <NavBarItem
        style={item.pathname === location.pathname ? currentItemAnimation : {}}
        key={item.id}
        active={item.pathname === location.pathname}
        onClick={() => handleItemClick(item.pathname)}
      >
        <NavBarItemIcon
          height={25}
          fill={
            item.pathname === location.pathname
              ? theme.navbar.icon.selected
              : theme.navbar.icon.unselected
          }
        >
          {getIcon(item.iconName)}
        </NavBarItemIcon>
      </NavBarItem>
    ));
  }
  
  return (
    <NavBarContainer>
      <NavBarList>
        {renderItems()}
        <ProfileItem
          onClick={async () => {
            await logOut();
            navigate(PATHS.LOG_IN);
          }}
        >
          <ProfileImage src={NavbarProfileDefaultImg} alt="user profile" />
        </ProfileItem>
      </NavBarList>
    </NavBarContainer>
  );
};

export default Navbar;

const NavBarContainer = styled.nav`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 90%;
  align-self: center;
  position: fixed;
  bottom: 20px;
`;

const NavBarList = styled.ul`
  display: flex;
  justify-content: space-between;
  align-items: center;
  background-color: ${(props) => props.theme.navbar.background};
  bottom: 20px;
  height: 80px;
  width: 100%;
  list-style: none;
  border-radius: 24px;
  padding: 0 23px 0 12px;
`;

const NavBarItem = animated(styled.li<NavBarItemProps>`
  background-color: ${(props) =>
    props.active ? props.theme.navbar.selected : props.theme.navbar.background};
  width: 60px;
  height: 60px;
  border-radius: 16px;
`);

const ProfileItem = styled.li`
  text-align: center;
`;

const NavBarItemIcon = styled(SvgIconWrapper)`
  background-color: transparent;
  text-align: center;
  margin-top: 15px; //this is wrong, but i connot keep losing time!
`;

const ProfileImage = styled.img`
  background-color: transparent;
`;

