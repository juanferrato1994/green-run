import styled from "styled-components";
import SvgIconWrapper from "../SvgIconWrapper";

import { MouseEventHandler, ReactNode } from "react";
import { SquareIconButton } from "../common";

interface ThemeButtonProps {
  className?: string;
  onClick: MouseEventHandler<HTMLButtonElement>;
  fill: string;
  iconHeight: number;
  children: ReactNode;
}

const ThemeButton = ({
  className,
  onClick,
  fill,
  iconHeight,
  children,
}: ThemeButtonProps) => {
  return (
    <ButtonContainer className={className} onClick={onClick}>
      <Icon height={iconHeight} fill={fill}>
        {children}
      </Icon>
    </ButtonContainer>
  );
};

export default ThemeButton;

const ButtonContainer = styled.button`
  border: none;
  background: transparent;
`;

const Icon = styled(SvgIconWrapper)`
  ${SquareIconButton};
  background: ${(props) => props.theme.themeButtonBackground};
`;
