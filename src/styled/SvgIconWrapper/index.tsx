import styled from "styled-components";


const SvgIconWrapper = styled.div.attrs(
  (props: { height: number; fill: string; stroke?: string }) => props
)`
  svg {
    height: ${(props) => props.height}px;
    fill: ${(props) => props.fill};
    stroke: ${(props) => props.stroke};
  }
`;

export default SvgIconWrapper;
