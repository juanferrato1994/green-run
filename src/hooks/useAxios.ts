import { useState, useEffect } from "react";
import axios, { AxiosError, Method } from "axios";

interface useAxiosProps {
  url: string;
  method?: Method;
}

const useAxios = ({ url, method = "get" }: useAxiosProps) => {
  const [data, setData] = useState([]);
  const [error, setError] = useState<string | null>(null);
  const [loading, setLoading] = useState(false);

  const fetchData = async () => {
    const config = {
      method: method.toLowerCase(),
      url: url,
    };
    try {
      setLoading(true);
      const res = await axios(config);
      setData(res.data);
    } catch (e) {
      const error = e as Error | AxiosError;
      setError(error.message);
    } finally {
      setLoading(false);
    }
  };

  useEffect(() => {
    const controller = new AbortController();

    fetchData();
    return () => controller.abort();

    // eslint-disable-next-line
  }, []);

  return { data, error, loading };
};

export default useAxios;
