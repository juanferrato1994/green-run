import { ReactComponent as Heart } from "./svg//heart.svg";
import { ReactComponent as Cross } from "./svg/cross.svg";
import { ReactComponent as House } from "./svg/house.svg";
import { ReactComponent as Timer } from "./svg/timer.svg";
import { ReactComponent as Notepad } from "./svg/notepad.svg";

export { Heart, Cross, House, Timer, Notepad };