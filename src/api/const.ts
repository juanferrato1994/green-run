export const SPORTS_API = {
  BASE_URL: process.env.REACT_APP_SPORTS_BASE_URL,
  ENDPOINTS: {
    ALL_SPORTS: "/all_sports.php",
  },
};
