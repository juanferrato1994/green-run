import styled, { useTheme } from "styled-components";
import { PrimarySubtitle } from "../../styled/PrimarySubtitle";
import { PrimaryTitle } from "../../styled/PrimaryTitle";
import SvgIconWrapper from "../../styled/SvgIconWrapper";
import { useContext, useEffect, useState } from "react";
import GoBackArrow from "../../styled/GoBackArrow";
import Navbar from "../../styled/NavBar";
import { AuthContext } from "../../contexts/AuthContext";
import { getReactionHistory } from "../../services/history";
import { Heart, Cross } from "../../assets/icons";
import { useTrail, animated } from "react-spring";

const WORDING = {
  title: "History",
  subtitle: "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
};

const History = () => {
  const theme = useTheme();
  const [historyItems, setHistoryItems] = useState<any[]>([]);
  const { title, subtitle } = WORDING;
  const { user } = useContext(AuthContext);

  const trail = useTrail(historyItems.length, {
    to: { opacity: 1, scale: 1 },
    from: { opacity: 0, scale: 0 },
    config: {
      duration: 100
    }
  });

  const handleHistoryFetch = async () => {
    if (!user) return;
    const historyReactionList = await getReactionHistory({ uid: user?.uid });
    setHistoryItems(historyReactionList);
  };

  useEffect(() => {
    handleHistoryFetch();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [user]);

  return (
    <Container>
      <Content>
        <GoBackArrow />
        <Title text={title} />
        <Subtitle text={subtitle} />
          <HistoryList>
            {trail.map((props: any, i: number) => (
              <Item
                style={props}
                key={historyItems[i].sport.idSport + historyItems[i].date}
              >
                <BackgroundWrapper />
                <ItemImage
                  backgroundUrl={historyItems[i].sport.strSportThumb}
                />
                <ItemImageText text={historyItems[i].sport.strSport} />
                <ItemIcon
                  height={24}
                  fill={
                    historyItems[i].reaction === "like"
                      ? theme.icons.like
                      : theme.icons.dislike
                  }
                >
                  {historyItems[i].reaction === "like" ? <Heart /> : <Cross />}
                </ItemIcon>
              </Item>
            ))}
          </HistoryList>
      </Content>
      <Navbar />
    </Container>
  );
};

export default History;

const Container = styled.main`
  display: flex;
  flex-direction: column;
  background-color: ${(props) => props.theme.background.dark};
  height: 100vh;
  position: relative;
`;

const Content = styled.div`
  padding: 20px 32px;
`;

const Title = styled(PrimaryTitle)`
  text-align: left;
`;

const Subtitle = styled(PrimarySubtitle)`
  text-align: left;
`;

const Item = animated(styled.li`
  height: 77px;
  position: relative;
  background: ${(props) => props.theme.background.item};
  display: flex;
  border-radius: 12px;
  margin: 7px 0;
`);

const ItemImage = styled.div<{ backgroundUrl: string }>`
  object-fit: cover;
  background: url(${(props) => props.backgroundUrl});
  width: 80%;
  opacity: 0.51;
  border-radius: 12px;
`;

const BackgroundWrapper = animated(styled.div`
  position: absolute;
  background-color: rgb(0, 0, 0);
  border-radius: 12px;
  width: 80%;
  height: 100%;
`);

const ItemImageText = styled(PrimarySubtitle)`
  align-self: center;
  position: absolute;
  font-size: 24px;
  margin-left: 15px;
  font-weight: 700;
  line-height: normal;
  color: ${(props) => props.theme.candidateNameText};
`;

const ItemIcon = styled(SvgIconWrapper)`
  flex: 1;
  background-color: transparent;
  align-items: center;
  text-align: center;
  align-self: center;
`;

const HistoryList = styled.ul`
  scroll-behavior: normal;
  overflow: scroll;
  margin: 14px 0;
  max-height: 60vh;
`;
