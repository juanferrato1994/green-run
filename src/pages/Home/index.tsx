import React, { useContext, useState } from "react";
import styled, { useTheme } from "styled-components";
import { ThemeContext } from "../../contexts/ThemeStore";
import DislikeButton from "../../styled/DislikeButton";
import LikeButton from "../../styled/LikeButton";
import Navbar from "../../styled/NavBar";
import ThemeButton from "../../styled/ThemeButton";
import { ReactComponent as Sun } from "../../assets/svg/sun.svg";
import { ReactComponent as Moon } from "../../assets/svg/moon.svg";
import { SquareIconButton } from "../../styled/common";
import { SportsContext } from "../../contexts/SportsContext";
import { PrimarySubtitle } from "../../styled/PrimarySubtitle";
import { AuthContext } from "../../contexts/AuthContext";
import { Reaction, registerReaction } from "../../services/history";
import { useSpring, animated } from "react-spring";
import SvgIconWrapper from "../../styled/SvgIconWrapper";
import { Heart, Cross } from "../../assets/icons";

const minSwipeDistance = 100;
const MAX_ITEMS = 2; //some items have not available data
// const defaultImage = require("/assets/jpg/default-sport-image.jpg").default;

function Home() {
  const theme = useTheme();
  const { user } = useContext(AuthContext);
  const { toggleTheme } = useContext(ThemeContext);
  const { sportList, loadingSports } = useContext(SportsContext);
  const [currentSportPosition, setCurrentSportPosition] = useState<number>(0);
  const [isListComplete, setIsListComplete] = useState<boolean>(false);

  const [touchStart, setTouchStart] = useState<null | number>(null);
  const [touchEnd, setTouchEnd] = useState<null | number>(null);
  const [currentReaction, setCurrentReaction] = useState<
    "like" | "dislike" | ""
  >("");

  const [showReaction, setShowReaction] = useState(false);

  // const [animateReaction, setAnimateReaction] = useState<boolean>(false);

  // const fadeInAnimation = useSpring({
  //   from: { opacity: 0 },
  //   to: { opacity: 1 },
  //   config: config.gentle,
  // });

  const reactionAnimation = useSpring({
    from: {
      opacity: showReaction ? 0 : 1,
      height: showReaction ? "20px" : "40px",
    },
    to: {
      opacity: showReaction ? 1 : 0,
      height: showReaction ? "40px" : "20px",
    },
  });

  // const reactionAnimation = useTransition(animateReaction, {
  //   from: { opacity: 1 },
  //   enter: { opacity: 0 },
  //   leave: { opacity: 1 },
  //   reverse: animateReaction,
  //   delay: 200,
  //   config: config.molasses,
  //   onRest: () => setAnimateReaction(false),
  // });

  const handleNextSportItem = () => {
    setCurrentSportPosition((currentPosition) => {
      if (currentPosition <= MAX_ITEMS) {
        return (currentPosition += 1);
      }
      setIsListComplete(true);
      return currentPosition;
    });
  };

  const onTouchStart = (e: any) => {
    if (showReaction) return;
    setTouchEnd(null);
    setTouchStart(e.targetTouches[0].clientX);
  };

  const onTouchMove = (e: any) => setTouchEnd(e.targetTouches[0].clientX);

  const onTouchEnd = () => {
    if (showReaction) return;
    if (!touchStart || !touchEnd) return;
    const distance = touchStart - touchEnd;
    const isLeftSwipe = distance > minSwipeDistance;
    const isRightSwipe = distance < -minSwipeDistance;
    if (isLeftSwipe || isRightSwipe) {
      isLeftSwipe ? handleLeftSwipe() : handleRightSwipe();
    }
  };

  const handleLeftSwipe = async () => {
    if (!user) return;
    const data = {
      sport: { ...sportList.sports[currentSportPosition] },
      reaction: "dislike" as Reaction,
    };
    setCurrentReaction("dislike");
    registerReaction({ uid: user?.uid, data: data });
    setShowReaction(true);
    await new Promise((resolve) => setTimeout(resolve, 3000));
    setShowReaction(false);
    handleNextSportItem();
  };

  const handleRightSwipe = async () => {
    if (!user) return;
    const data = {
      sport: { ...sportList.sports[currentSportPosition] },
      reaction: "like" as Reaction,
    };
    setCurrentReaction("like");
    registerReaction({ uid: user?.uid, data: data });
    setShowReaction(true);
    await new Promise((resolve) => setTimeout(resolve, 3000));
    setShowReaction(false);
    handleNextSportItem();
  };

  const renderThemeIcon = () => (theme.name === "dark" ? <Sun /> : <Moon />);

  if (loadingSports) return <div>Loading...</div>;

  return (
    <Container>
      {!isListComplete ? (
        <>
          <CurrentCandidateSection
            onTouchStart={onTouchStart}
            onTouchMove={onTouchMove}
            onTouchEnd={onTouchEnd}
          >
            <>
              <ReactionIcon
                height={80}
                fill={
                  currentReaction === "like"
                    ? theme.icons.like
                    : theme.icons.dislike
                }
                style={reactionAnimation}
              >
                {currentReaction === "like" ? (
                  <Heart />
                ) : currentReaction === "dislike" ? (
                  <Cross />
                ) : (
                  <></>
                )}
              </ReactionIcon>
              <CandidateImage
                src={
                  sportList?.sports &&
                  sportList.sports[currentSportPosition].strSportThumb
                }
                alt={
                  sportList?.sports &&
                  sportList.sports[currentSportPosition].strSport
                }
              />
              <CandidateNameContainer>
                <CandidateName
                  text={
                    sportList?.sports &&
                    sportList.sports[currentSportPosition].strSport
                  }
                />
              </CandidateNameContainer>
            </>
          </CurrentCandidateSection>
          <ThemeToggleButton
            onClick={toggleTheme}
            iconHeight={35}
            fill={theme.icons.like}
          >
            {renderThemeIcon()}
          </ThemeToggleButton>
          <CandidateIconContainer>
            <CandidateIconImage
              src={
                sportList?.sports &&
                sportList?.sports[currentSportPosition]?.strSportIconGreen
              }
            />
          </CandidateIconContainer>
          <ButtonsContainer>
            <DislikeButton onPress={handleLeftSwipe} />
            <LikeButton onPress={handleRightSwipe} />
          </ButtonsContainer>
        </>
      ) : (
        <CandidatesDepletedSection>
          <PrimarySubtitle text="You are up to date!" />
        </CandidatesDepletedSection>
      )}
      <Navbar />
    </Container>
  );
}

export default Home;
const Container = styled.main`
  display: flex;
  flex-direction: column;
  background-color: ${(props) => props.theme.background.dark};
  height: 100vh;
  position: relative;
`;

const ButtonsContainer = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  margin: 47px;
`;

const CurrentCandidateSection = styled.div`
  position: relative;
  margin-bottom: 50px;
`;

const CandidateImage = animated(styled.img`
  object-fit: contain;
  background-color: #181828;
  height: 50vh;
  max-width: 100vw;
`);

const CandidateName = styled(PrimarySubtitle)`
  position: absolute;
  font-size: 34px;
  font-weight: bold;
  left: 21px;
  bottom: 21px;
  color: ${(props) => props.theme.candidateNameText};
`;

const CandidateNameContainer = styled.div`
  height: 100px;
  width: 100%;
  bottom: -50px;
  position: absolute;
  background: linear-gradient(
    360deg,
    #000000 0%,
    #000000 58.85%,
    rgba(0, 0, 0, 0) 100%
  );
  border-radius: 0px 0px 32px 32px;
`;

const CandidateIconContainer = styled.div`
  ${SquareIconButton};
  background: rgba(34, 34, 67, 0.2);
  backdrop-filter: blur(10px);
  position: absolute;
  top: 22px;
  right: 21px;
`;

const CandidateIconImage = styled.img`
  height: 26px;
  filter: invert(34%) sepia(100%) saturate(4642%) hue-rotate(213deg)
    brightness(99%) contrast(80%);
`;

const ThemeToggleButton = styled(ThemeButton)`
  position: absolute;
  top: 22px;
  left: 21px;
`;

const CandidatesDepletedSection = animated(styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  height: 100%;
`);

const ReactionIcon = animated(styled(SvgIconWrapper)`
  position: absolute;
  opacity: 0;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
`);
