import React from "react";
import { useNavigate } from "react-router-dom";
import styled from "styled-components";
import PrimaryButton from "../../styled/PrimaryButton";
import { PATHS } from "../../navigation/paths";
import { PrimaryTitle } from "../../styled/PrimaryTitle";
import { PrimarySubtitle } from "../../styled/PrimarySubtitle";
import { OnboardingImage } from "../../assets/images";
import { useSpring, animated } from "react-spring";

const WORDING = {
  title: "Discover Your Best Sport With Us",
  subtitle: "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
  loginButton: "Login",
};

const Onboarding = () => {
  const navigate = useNavigate();

  const onboardingImageProps = useSpring({
    to: { opacity: 1, left: "-30px" },
    from: { opacity: 0, left: "-190px" },
    delay: 1500,
    immediate: true,
  });
  const props = useSpring({
    to: { height: "43vh" },
    from: { height: "0vh" },
    delay: 200,
    immediate: true,
    config: {
      duration: 1000,
    },
  });


  const {title, subtitle, loginButton} = WORDING;

  function handleLoginClick(e: React.MouseEvent<HTMLButtonElement>) {
    navigate(PATHS.LOG_IN);
  }
  return (
    <Container>
      <Image style={onboardingImageProps} src={OnboardingImage} alt="Lionel Messi" />
      <BottomBanner style={props}>
        <BottomBannerContent>
          <div>
            <BottomBannerTitle text={title} />
            <BottomBannerSubtitle text={subtitle} />
          </div>
          <LoginButton text={loginButton} onClick={handleLoginClick} />
        </BottomBannerContent>
      </BottomBanner>
    </Container>
  );
};

export default Onboarding;

const Container = styled.div`
  display: flex;
  justify-content: center;
  flex-direction: column;
  background-color: #232232;
  height: 100vh;
`;

const Image = animated(styled.img`
  position: absolute;
  top: 35px;
  left: -30px;
`);

const BottomBanner = animated(styled.div`
  max-height: 346px;
  width: 100vw;
  position: absolute;
  background-color: ${(props) => props.theme.bottomBanner};
  bottom: 0;
  border-radius: 36px 36px 0 0;
  display: flex;
`);

const BottomBannerContent = animated(styled.div`
  padding: 46px 32px;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  flex: 1;
`);

const BottomBannerTitle = styled(PrimaryTitle)`
  font-size: 28px;
  text-align: left;
`;

const BottomBannerSubtitle = styled(PrimarySubtitle)`
  margin-bottom: 16px;
  margin: 12px 0;
`;

const LoginButton = styled(PrimaryButton)`
  align-self: start;
`;
