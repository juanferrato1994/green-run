import { createContext } from "react";
import { SPORTS_API } from "../api/const";
import useAxios from "../hooks/useAxios";
import { Sport } from "../types";


interface SportsState {
  sportList: { sports: Sport[] | [] } | any;
  loadingSports: boolean;
  sportsError: null | string;
}

const SportsContext = createContext<SportsState>(undefined!);

const SportsProvider = ({ children }: any) => {
  const {
    BASE_URL,
    ENDPOINTS: { ALL_SPORTS },
  } = SPORTS_API;

  const {
    data: sportList,
    loading: loadingSports,
    error: sportsError,
  } = useAxios({ url: BASE_URL + ALL_SPORTS });

  return (
    <SportsContext.Provider
      value={{ sportList, loadingSports, sportsError }}
    >
      {children}
    </SportsContext.Provider>
  );
};

export { SportsContext, SportsProvider };
