import { createContext, useState } from "react";
import { dark, light } from "../themes/themes";

const ThemeContext = createContext({ theme: dark, toggleTheme: () => {} });

const ThemeStore = ({ children }: any) => {
  const [theme, setTheme] = useState(dark);

  const toggleTheme = () => {
    setTheme((currentTheme: { name: string }) =>
      currentTheme.name === "dark" ? light : dark
    );
  };

  return (
    <ThemeContext.Provider value={{ toggleTheme, theme }}>
      {children}
    </ThemeContext.Provider>
  );
};

export { ThemeContext, ThemeStore };
